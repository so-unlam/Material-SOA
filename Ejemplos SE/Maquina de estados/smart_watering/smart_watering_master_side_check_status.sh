
report()
{
  local file="/tmp/report_status_sw.txt"
  echo "" > "$file"
  date +"%d/%m/%Y %H:%M:%S" >> "$file"
  echo "$1" >> "$file"
  echo "" >> "$file"
  return 0
}

addr_device="98:D3:61:F9:39:A5"
device="/dev/rfcomm0"
baudrate="115200"
bluetoothctl devices | grep -q "$addr_device"
if test $? -ne 0
then
  rc=0
  sudo rfcomm release "$device" "$addr_device"
  sudo bluetoothctl remove "$addr_device"
  sudo bluetoothctl --timeout 9 scan on
  rc=$(expr "$rc" + "$?")
  sudo bluetoothctl --timeout 5 pair "$addr_device"
  rc=$(expr "$rc" + "$?")
  sudo bluetoothctl --timeout 5 trust "$addr_device"
  rc=$(expr "$rc" + "$?")
  sudo rfcomm bind "$device" "$addr_device"
  rc=$(expr "$rc" + "$?")

  if test "$rc" -ne 0
  then
    echo "ERROR configurando dispositivo bluetooth [$addr_device]..."
    exit 1
  fi
fi

if test -c "$device"
then
  python smart_watering_master_side_check_status.py "$device" "$baudrate"
  if test $? -ne 0
  then
    report "ERROR"
    cd /home/pi/bin/casa;sh apagar_luces.sh
    sleep 3
    cd /home/pi/bin/casa;sh encender_luces.sh
  else
    report "OK"
  fi
  exit $?
else
  exit 2
fi
