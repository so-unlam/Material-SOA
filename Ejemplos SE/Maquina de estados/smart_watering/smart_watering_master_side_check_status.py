#!/usr/bin/env python
import serial
import sys
import time
import datetime

from time import sleep
from select import select
from pathvalidate import ValidationError, validate_filename, validate_filepath

device_serial="/dev/rfcomm0"
device_baudrate=9600
header_cmd="HEADER:::"

def openSerialBT(timeout_ = 5):
  try:
    global bluetoothSerial
    bluetoothSerial = serial.Serial(device_serial, device_baudrate, timeout = timeout_)
  except:
    print("Error abriendo puerto serial bluetooth...")
    print("Quitting...")
    sys.exit(1)
  
def closeSerialBT():
  try:
    bluetoothSerial.write("bye")
    bluetoothSerial.close()
    return 0
  except Exception as e:
    print(e)
    return 1

def writeSerialBT(string_ = ""):
  try:
    bluetoothSerial.write(string_)
  except Exception as e:
    print(e)
    print("Error escribiendo en puerto serial bluetooth...")
    print("Quitting...")
    sys.exit(1)

def readSerialBT(string_ = ""):
  try:
    data = bluetoothSerial.readline()
    data = data.rstrip()
    if data:
      if string_:
        if string_==data:
          return data
        else:
          return ""
      else:
        return data
    return ""
  except Exception as e:
    print(e)
    print("Error leyendo de puerto serial bluetooth...")
    print("Quitting...")
    sys.exit(1)

def main():
  openSerialBT(20)
  writeSerialBT(header_cmd + "ACK")
  data = readSerialBT()
  if data == "":
    return 4
  closeSerialBT()
  return 0

if __name__ == '__main__':
  n = len(sys.argv)
  if n >= 1:
    try:
      validate_filepath(sys.argv[1], "Linux")
      device_serial=sys.argv[1]
    except ValidationError as e:
      print(e)
      sys.exit(1)
  if n >= 2:
    try:
      device_baudrate = int(sys.argv[2])
    except:
      print('El segundo parametro debe ser los baudrate del puerto serial, debe ser un numero. Ejemplo: 9600')
      sys.exit(1)
  sys.exit(main())
