#!/usr/bin/env python
import serial
import sys
import time
import datetime

from time import sleep
from select import select
from pathvalidate import ValidationError, validate_filename, validate_filepath

device_serial="/dev/rfcomm0"
device_baudrate=9600
header_cmd="HEADER:::"

def readInput():
  try:
    s=""
    timeout = 0.1
    rlist, _, _ = select([sys.stdin], [], [], timeout)
    if rlist:
      s = sys.stdin.readline()
      s = s.rstrip()
      if s == "":
        s="none"
  except:
    s="quit"
  return s

def openSerialBT(timeout_ = 5):
  try:
    global bluetoothSerial
    bluetoothSerial = serial.Serial(device_serial, device_baudrate, timeout = timeout_)
  except:
    print("Error abriendo puerto serial bluetooth...")
    print("Quitting...")
    sys.exit(1)
  
def closeSerialBT():
  try:
    bluetoothSerial.write("bye")
    bluetoothSerial.close()
    return 0
  except Exception as e:
    print(e)
    return 1

def writeSerialBT(string_ = ""):
  try:
    bluetoothSerial.write(string_)
  except Exception as e:
    print(e)
    print("Error escribiendo en puerto serial bluetooth...")
    print("Quitting...")
    sys.exit(1)

def readSerialBT(string_ = ""):
  try:
    data = bluetoothSerial.readline()
    data = data.rstrip()
    if data:
      if string_:
        if string_==data:
          return data
        else:
          return ""
      else:
        return data
    return ""
  except Exception as e:
    print(e)
    print("Error leyendo de puerto serial bluetooth...")
    print("Quitting...")
    sys.exit(1)

def help():
  print("Con esa utilidad usted podra: ")
  print("Enviar y recibir un comandos al puerto serial bluetooth vinculado cuyo dispositivo es (%s)."%(device_serial))
  print("Cuya configuracion es (%s)."%(device_baudrate))
  sys.stdout.write("$ ")
  sys.stdout.flush()

def main():
  print("Inicializando...")
  openSerialBT(10)
  writeSerialBT(header_cmd + "ACK")
  data = readSerialBT()
  if data == "":
    print("Error al inicializar puerto serial bluetooth...")
    print("Quitting...")
    return 4
  print(data)
  closeSerialBT()
  time.sleep(1)
  openSerialBT(2)
  print("Escriba \"quit\" para salir")
  print("Escriba \"?\" para ayuda")
  sys.stdout.write("$ ")
  sys.stdout.flush()

  while True:
    try:
      msg_to_send = readInput()
      if msg_to_send == "quit" or msg_to_send == "q" or msg_to_send == "Q":
        break
      if msg_to_send == "?":
        help()
        continue
      if msg_to_send == "none":
        sys.stdout.write("$ ")
        sys.stdout.flush()
        msg_to_send=""
      cts=0
      if msg_to_send:
        cts = datetime.datetime.now()
        print('$ %s: enviando(%s)'%(cts, msg_to_send))
        writeSerialBT(header_cmd + msg_to_send)
        cts = datetime.datetime.now()
        print('$ %s: enviado(%s)'%(cts, msg_to_send))
        sys.stdout.write("$ ")
        sys.stdout.flush()
      data = readSerialBT()
      if data:
        ctr = datetime.datetime.now()
        if cts != 0:
          diff = ctr - cts
          print('%s: diff(%s) - recibido(%s)'%(ctr, diff, data))
        else:
          print('%s: recibido(%s)'%(ctr, data))
        sys.stdout.write("$ ")
        sys.stdout.flush()
    except SystemExit:
      sys.exit(1)
    except KeyboardInterrupt:
      break
    except:
      pass
  print("Quitting...")
  closeSerialBT()
  return 0

if __name__ == '__main__':
  n = len(sys.argv)
  if n >= 1:
    try:
      validate_filepath(sys.argv[1], "Linux")
      device_serial=sys.argv[1]
    except ValidationError as e:
      print(e)
      sys.exit(1)
  if n >= 2:
    try:
      device_baudrate = int(sys.argv[2])
    except:
      print('El segundo parametro debe ser los baudrate del puerto serial, debe ser un numero. Ejemplo: 9600')
      sys.exit(1)
  sys.exit(main())

