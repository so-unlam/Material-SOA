
#ifndef __SMART_WATERING_EVENT_TYPES__
#define __SMART_WATERING_EVENT_TYPES__

  extern void print                    (String str);
  extern void debug_print              (String str);
  extern void print_current_state      ();
  extern void reset                    ();

  extern SoftwareSerial BT;

  extern bool IS_DEBUG_ENABLED;

  const short pin_liquid_sensor         = 6;
  const short pin_raining_sensor        = 7;

  #define SERIAL_ENABLED                0

  #if SERIAL_ENABLED
    #define SerialPrint(str)\
      {\
        Serial.println(str);\
      }
  #else
    #define SerialPrint(str)
  #endif

  String HEADER_BT_CMD = "HEADER:::";

  #define TS_CHANGE_STATE_DEV           700       //700  milisegundos
  #define TS_LIMIT_PUMP_RUNNING_TIME    (10000*1) //10   segundos
  #define TS_LIMIT_PUMP_REST_TIME       (30000*1) //30   segundos

  #define CMD_ACK                       "ACK"
  #define CMD_PUMP_OFF                  "POF"
  #define CMD_PUMP_ON                   "PON"
  #define CMD_RST                       "RST"
  #define CMD_CFG_BT                    "CBT"
  #define CMD_LST_CMD                   "LCM"
  #define CMD_DBG_ON                    "DON"
  #define CMD_DBG_OFF                   "DOF"
  #define CMD_GET_STATE                 "GST"
  #define CMD_ERROR                     "ERR"
  #define CMD_OK                        "OK"

  #define MAX_TYPE_EVENTS               6
  #define MAX_EVENTS                    10

  long ts_liquid_sensor_state           = 0;
  long ts_raining_sensor_state          = 0;
  long ts_running_pump                  = 0;
  long ts_rest_pump                     = 0;

  enum events          { EV_CONT ,  EV_RNG     , EV_RNNG         , EV_CSP             , EV_CSTP           , EV_LWD              , EV_NLD                 , EV_PRTHE                          , EV_PRETHE                      , EV_UNK      } new_event;
  String events_s [] = {"EV_CONT", "EV_RAINING", "EV_NOT_RAINING", "EV_CMD_START_PUMP", "EV_CMD_STOP_PUMP", "EV_LIQUID_DETECTED", "EV_NO_LIQUID_DETECTED", "EV_PUMP_RUNNING_TIME_HAS_EXPIRED", "EV_PUMP_REST_TIME_HAS_EXPIRED", "EV_UNKNOW" };
  
  bool procesar_cmd     (String cmd, bool btSerial=false);
  bool water_sensor     (bool force, unsigned long ct);
  bool raining_sensor   (bool force, unsigned long ct);
  bool bt_cmd           (bool force, unsigned long ct);
  bool serial_cmd       (bool force, unsigned long ct);
  bool ch_ts_run_pump   (bool force, unsigned long ct);
  bool ch_ts_rest_pump  (bool force, unsigned long ct);

  typedef bool (*eventType)(bool force = true, unsigned long ct = 0);
  eventType event_type[MAX_TYPE_EVENTS] = {water_sensor, raining_sensor, bt_cmd, serial_cmd, ch_ts_run_pump, ch_ts_rest_pump};
  
  bool water_sensor(bool force, unsigned long ct)
  {
    if(ct == 0)
    {
      ct = millis();
    }

    int diff = ((force)?(TS_CHANGE_STATE_DEV):(ct - ts_liquid_sensor_state));

    if(diff >= TS_CHANGE_STATE_DEV)
    {
      ts_liquid_sensor_state = ct;

      byte lectura_sensor = digitalRead(pin_liquid_sensor);
      if(lectura_sensor == HIGH)
      {
        new_event = EV_LWD;
        return true;
      }
      else
      {
        new_event = EV_NLD;
        return true;
      }
    }
    return false;
  }

  bool raining_sensor(bool force, unsigned long ct)
  {
    if(ct == 0)
    {
      ct = millis();
    }

    int diff = ((force)?(TS_CHANGE_STATE_DEV):(ct - ts_raining_sensor_state));

    if(diff >= TS_CHANGE_STATE_DEV)
    {
      ts_raining_sensor_state = ct;

      byte lectura_sensor = digitalRead(pin_raining_sensor);
      if(lectura_sensor == LOW)
      {
        new_event = EV_RNG;
        return true;
      }
      else
      {
        new_event = EV_RNNG;
        return true;
      }
    }
    return false;
  }

  bool bt_cmd(bool force, unsigned long ct)
  {
    BT.flush();
    if(BT.available() > 0)
    {
      String msg = BT.readString();
      BT.flush();
      msg.trim();
      String header_cmd = msg.substring(0, HEADER_BT_CMD.length());
      String cmd = msg.substring(HEADER_BT_CMD.length(), HEADER_BT_CMD.length()+3);
      if((header_cmd == HEADER_BT_CMD) && (cmd.length() > 0))
      {
        if(procesar_cmd(cmd, true))
        {
          print("CMD PROCESADO");
          return true;
        }
        return false;
      }
    }
    return false;
  }

  bool serial_cmd(bool force, unsigned long ct)
  {
    #if SERIAL_ENABLED
      if(Serial.available() > 0)
      {
        String msg = Serial.readString();
        msg.trim();
        if(msg.length() > 0)
        {
          print("CMD PROCESADO");
          return procesar_cmd(msg, false);
        }
      }
    #endif
    return false;
  }

  bool ch_ts_run_pump(bool force, unsigned long ct)
  {
    if(ts_running_pump > 0)
    {
      if(ct == 0)
      {
        ct = millis();
      }
      int diff = ((force)?(TS_LIMIT_PUMP_RUNNING_TIME):(ct - ts_running_pump));

      SerialPrint("ch_ts_run_pump");
      SerialPrint(diff);

      if(diff >= TS_LIMIT_PUMP_RUNNING_TIME)
      {
        ts_running_pump = -1;
        new_event = EV_PRTHE;
        return true;
      }
    }
    return false;
  }

  bool ch_ts_rest_pump(bool force, unsigned long ct)
  {
    if(ts_rest_pump > 0)
    {
      if(ct == 0)
      {
        ct = millis();
      }
      int diff = ((force)?(TS_LIMIT_PUMP_REST_TIME):(ct - ts_rest_pump));
      SerialPrint("ch_ts_rest_pump");
      SerialPrint(diff);

      if(diff >= TS_LIMIT_PUMP_REST_TIME)
      {
        ts_rest_pump = -1;
        new_event = EV_PRETHE;
        return true;
      }
      return false;
    }
  }

  void configure_bt()
  {
    BT.println("AT");
    delay(1000);
    SerialPrint(BT.readString());
    BT.println("AT+NAME:SMARTWATERING");
    delay(1000);
    SerialPrint(BT.readString());
    BT.println("AT+UART:115200,1,0"); // 115200
    delay(1000);
    SerialPrint(BT.readString());
    BT.println("AT+PSWD=\"0000\"");
    delay(1000);
    SerialPrint(BT.readString());
    SerialPrint("TERMINADO");
    delay(3000);
    reset();
  }

  bool procesar_cmd(String cmd, bool btSerial=false)
  {
    if(cmd == CMD_ACK)
    {
      print(CMD_OK);
    }
    else if(cmd == CMD_PUMP_OFF)
    {
      new_event = EV_CSTP;
      print(CMD_OK);
      return true;
    }
    else if(cmd == CMD_PUMP_ON)
    {
      new_event = EV_CSP;
      print(CMD_OK);
      return true;
    }
    else if(cmd == CMD_RST)
    {
      print(CMD_OK);
      reset();
      return true;
    }
    else if(cmd == CMD_CFG_BT)
    {
      if(! btSerial)
      {
        print(CMD_OK);
        configure_bt();
      }
      else
      {
        print(CMD_ERROR);
      }
    }
    else if(cmd == CMD_DBG_ON)
    {
      IS_DEBUG_ENABLED=true;
      print(CMD_OK);
    }
    else if(cmd == CMD_DBG_OFF)
    {
      IS_DEBUG_ENABLED=false;
      print(CMD_OK);
    }
    else if(cmd == CMD_GET_STATE)
    {
      print_current_state();
      print(CMD_OK);
    }
    else if(cmd == CMD_LST_CMD)
    {
      print("");
      print("Listado de comandos: ");
      print("CMD_ACK      =ACK"    );
      print("CMD_PUMP_OFF =POF"    );
      print("CMD_PUMP_ON  =PON"    );
      print("CMD_RST      =RST"    );
      print("CMD_CFG_BT   =CBT"    );
      print("CMD_LST_CMD  =LCM"    );
      print("CMD_DBG_ON   =DON"    );
      print("CMD_DBG_OFF  =DOF"    );
      print("CMD_GET_STATE=GST"    );
    }
    else
    {
      if(btSerial)
      {
        print("CMD := [" + cmd + "]");
        print(CMD_ERROR);
      }
      else
      {
        print("Serial CMD := [" + cmd + "]");
      }
    }
    return false;
  }

#endif
