
#ifndef __SMART_WATERING_STATE_MACHINE__
#define __SMART_WATERING_STATE_MACHINE__

  #include "smart_watering_event_types.h"

  #define MAX_STATES                    6
  #define TIME_DIFF_BETWEEN_EXEC_CYCLES 50 //50 milisegundos

  #define AC_ST_PUMP_STOPPED            0
  #define AC_ST_PUMP_ON                 1
  short pump_state                      = AC_ST_PUMP_STOPPED;

  short last_index_type_sensor          = 0;
  long  lct                             = 0;

  const short pin_pump                  = 8;

  extern void print                    (String str);
  extern void debug_print              (String str);
  extern void print_state_event        (String state, String event);
  extern void debug_print_state_event  ();

  void(* funcReset) (void) = 0;

  typedef void (*transition)();
  
  //Actions
  void reset      ();
  void error      ();
  void none       ();
  void raining    ();
  void liquid     ();
  void noliquid   ();
  void torunpump  ();
  void torest     ();
  void start_pump ();
  void stop_pump  ();
  
  enum states          { ST_INIT  ,  ST_IDLE      , ST_RAINING        , ST_DRAWING_WATER    , ST_PUMP_BREAK     , ST_ERROR    } current_state;
  String states_s [] = {"ST_INIT" , "ST_IDLE"     , "ST_RAINING"      , "ST_DRAWING_WATER"  , "ST_PUMP_BREAK"   , "ST_ERROR"  };

  enum states last_state;
  enum events last_event;
  
  transition state_table_actions[MAX_STATES][MAX_EVENTS] =
  {
    {none             , none          , none             , none             , none          , none             , none          , none          , none             , error    } , // state ST_INIT
    {none             , none          , none             , start_pump       , stop_pump     , liquid           , noliquid      , stop_pump     , none             , error    } , // state ST_IDLE
    {none             , none          , none             , start_pump       , stop_pump     , none             , none          , none          , none             , error    } , // state ST_RAINING
    {start_pump       , raining       , none             , none             , stop_pump     , none             , stop_pump     , torunpump     , none             , error    } , // state ST_DRAWING_WATER
    {none             , raining       , none             , none             , none          , none             , noliquid      , none          , torest           , error    } , // state ST_PUMP_BREAK
    {reset            , reset         , reset            , reset            , reset         , reset            , reset         , reset         , reset            , reset    }   // state ST_ERROR

    //EV_CONT         , EV_RNG        , EV_RNNG          , EV_CSP           , EV_CSTP       , EV_LWD           , EV_NLD        , EV_PRTHE      , EV_PRETHE        , EV_UNK
  };
  
  states state_table_next_state[MAX_STATES][MAX_EVENTS] =
  {
    {ST_IDLE          , ST_INIT       , ST_INIT          , ST_INIT          , ST_INIT       , ST_INIT          , ST_INIT       , ST_INIT       , ST_INIT          , ST_ERROR } , // state ST_INIT
    {ST_IDLE          , ST_RAINING    , ST_IDLE          , ST_DRAWING_WATER , ST_IDLE       , ST_DRAWING_WATER , ST_IDLE       , ST_IDLE       , ST_IDLE          , ST_ERROR } , // state ST_IDLE
    {ST_RAINING       , ST_RAINING    , ST_IDLE          , ST_RAINING       , ST_RAINING    , ST_RAINING       , ST_RAINING    , ST_RAINING    , ST_RAINING       , ST_ERROR } , // state ST_RAINING
    {ST_DRAWING_WATER , ST_RAINING    , ST_DRAWING_WATER , ST_DRAWING_WATER , ST_IDLE       , ST_DRAWING_WATER , ST_IDLE       , ST_PUMP_BREAK , ST_DRAWING_WATER , ST_ERROR } , // state ST_DRAWING_WATER
    {ST_PUMP_BREAK    , ST_PUMP_BREAK , ST_PUMP_BREAK    , ST_PUMP_BREAK    , ST_PUMP_BREAK , ST_PUMP_BREAK    , ST_IDLE       , ST_PUMP_BREAK , ST_IDLE          , ST_ERROR } , // state ST_PUMP_BREAK
    {ST_INIT          , ST_INIT       , ST_INIT          , ST_INIT          , ST_INIT       , ST_INIT          , ST_INIT       , ST_INIT       , ST_INIT          , ST_ERROR }   // state ST_ERROR

    //EV_CONT         , EV_RNG        , EV_RNNG          , EV_CSP           , EV_CSTP       , EV_LWD           , EV_NLD        , EV_PRTHE      , EV_PRETHE        , EV_UNK
  };

  void get_new_event( )
  {
    short index = 0;
    long ct = millis();
    long diff = (ct - lct);
    bool timeout = (diff > TIME_DIFF_BETWEEN_EXEC_CYCLES)? (true):(false);

    if(timeout)
    {
      timeout = false;
      lct     = ct;

      index = (last_index_type_sensor % MAX_TYPE_EVENTS);

      last_index_type_sensor++;

      if(event_type[index](false, ct))
      {
        return;
      }
    }
    new_event = EV_CONT;
  }

  void state_machine( )
  {
    get_new_event();
  
    if( (new_event >= 0) && (new_event < MAX_EVENTS) && (current_state >= 0) && (current_state < MAX_STATES) )
    {
      debug_print_state_event();
      
      state_table_actions[current_state][new_event]();
      current_state = state_table_next_state[current_state][new_event];

      last_event = new_event;
      last_state = current_state;
    }
    else
    {
      print_state_event(states_s[ST_ERROR], events_s[EV_UNK]);
    }
  }

  // Actions ...
  //---------------------------------------------------------------------------------------------------------
  void reset()
  {
    print("Reseting...");
    delay(800);
    funcReset();  
  }

  void error()
  {
    print("ACCION ERROR");
  }

  void none()
  {
  }

  void raining()
  {
    stop_pump();
    ts_running_pump = -1;
    ts_rest_pump    = -1;
  }

  void liquid()
  {
    start_pump();
    ts_running_pump = millis();
  }

  void noliquid()
  {
    stop_pump();
    ts_running_pump = -1;
    ts_rest_pump = -1;
  }

  void torunpump()
  {
    stop_pump();
    ts_running_pump = -1;
    ts_rest_pump = millis();
  }

  void torest()
  {
    ts_rest_pump = -1;
  }

  void start_pump() 
  {
    if(pump_state == AC_ST_PUMP_STOPPED)
    {
      digitalWrite(pin_pump, LOW);
      pump_state = AC_ST_PUMP_ON;
    }
  }

  void stop_pump() 
  {
    if(pump_state == AC_ST_PUMP_ON)
    {
      digitalWrite(pin_pump, HIGH);
      pump_state = AC_ST_PUMP_STOPPED;
    }
  }
  //---------------------------------------------------------------------------------------------------------

#endif
