#include <SoftwareSerial.h>
#include <Arduino.h>

#include "smart_watering_state_machine.h"

#define BLUETOOTH_BAUDRATE            115200
#define SERIAL_BAUDRATE               9600

bool IS_DEBUG_ENABLED                 = false;

const short pin_bt_tx                 = 4;
const short pin_bt_rx                 = 5;

SoftwareSerial BT(pin_bt_rx, pin_bt_tx);

void print(String str)
{
  BT.println(str);
  SerialPrint(str);
}

void print_state_event(String state, String event)
{
  String str = "ST-> [" + state + "]: " + "EVT-> [" + event + "].";
  print(str);
}

void debug_print(String str)
{
  if(IS_DEBUG_ENABLED)
  {
    print(str);
  }
}

void debug_print_state_event()
{
  if(IS_DEBUG_ENABLED)
  {
    if(new_event != EV_CONT)
    {
      if((last_event != new_event) || (last_state != current_state))
      {
        print_state_event(states_s[current_state], events_s[new_event]);
      }
    }
  }
}

void print_current_state()
{
  print_state_event(states_s[current_state], events_s[new_event]);
}

void setup() 
{
  BT.begin      (BLUETOOTH_BAUDRATE);
  Serial.begin  (SERIAL_BAUDRATE);
  pinMode       (pin_liquid_sensor , INPUT);
  pinMode       (pin_raining_sensor, INPUT);
  pinMode       (pin_pump          , OUTPUT);

  digitalWrite(pin_pump, HIGH);
  pump_state              = AC_ST_PUMP_STOPPED;

  lct                     = 0;
  ts_running_pump         = -1;
  ts_rest_pump            = -1;
  last_index_type_sensor  = 0;
  current_state           = ST_INIT;
  new_event               = EV_CONT;

  print("Starting...");
}

void loop() 
{
  state_machine();
}
