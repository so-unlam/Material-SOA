addr_device="98:D3:61:F9:39:A5"
device="/dev/rfcomm0"
baudrate="115200"
bluetoothctl devices | grep -q "$addr_device"
if test $? -ne 0
then
  rc=0
  sudo rfcomm release "$device" "$addr_device"
  sudo bluetoothctl remove "$addr_device"
  sudo bluetoothctl --timeout 9 scan on
  rc=$(expr "$rc" + "$?")
  sudo bluetoothctl --timeout 5 pair "$addr_device"
  rc=$(expr "$rc" + "$?")
  sudo bluetoothctl --timeout 5 trust "$addr_device"
  rc=$(expr "$rc" + "$?")
  sudo rfcomm bind "$device" "$addr_device"
  rc=$(expr "$rc" + "$?")

  if test "$rc" -ne 0
  then
    echo "ERROR configurando dispositivo bluetooth [$addr_device]..."
    exit 1
  fi
fi

if test -c "$device"
then
  python smart_watering_master_side_shell_commands.py "$device" "$baudrate"
  exit $?
else
  rc=0
  sudo rfcomm bind "$device" "$addr_device"
  rc=$(expr "$rc" + "$?")

  if test "$rc" -ne 0
  then
    echo "ERROR configurando dispositivo bluetooth [$addr_device]..."
    exit 1
  fi

  if test -c "$device"
  then
    python smart_watering_master_side_shell_commands.py "$device" "$baudrate"
    exit $?
  else
    echo "ERROR no existe el dispositivo bluetooth vinculado [$device]..."
    exit 2
  fi
fi
