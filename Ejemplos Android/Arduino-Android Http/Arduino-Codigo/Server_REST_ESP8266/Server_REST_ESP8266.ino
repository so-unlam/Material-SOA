#include <stdio.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

#define HTTP_REST_PORT 80
#define WIFI_RETRY_DELAY 500
#define MAX_WIFI_INIT_RETRY 50

#define PIN_LED 2
#define PIN_POTENTIOMETER A0

int sensor_value = 0;  // variable to store the value coming from the sensor

struct Led {
    byte status;
} led_resource;

const char* wifi_ssid = "Galileo proto";
const char* wifi_passwd = "Galileo2016";

ESP8266WebServer http_rest_server(HTTP_REST_PORT);

void init_led_resource()
{
    led_resource.status = LOW;
}

int init_wifi()
{
    int retries = 0;

    Serial.println("Conectando a WiFi AP..........");

    WiFi.mode(WIFI_STA);
    WiFi.begin(wifi_ssid, wifi_passwd);
    // check the status of WiFi connection to be WL_CONNECTED
    while ((WiFi.status() != WL_CONNECTED) && (retries < MAX_WIFI_INIT_RETRY))
    {
        retries++;
        delay(WIFI_RETRY_DELAY);
        Serial.print("#");
    }
    return WiFi.status(); // return the WiFi connection status
}

void get_method() 
{
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& jsonObj = jsonBuffer.createObject();
    char JSONmessageBuffer[200];

    sensor_value = analogRead(PIN_POTENTIOMETER);
    
    Serial.print("valor potenciomentro");
    Serial.println(sensor_value);
    
    jsonObj["valor_sensor"] = sensor_value;
    jsonObj.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    http_rest_server.send(200, "application/json", JSONmessageBuffer);
}

void json_to_resource(JsonObject& jsonBody)
{
    int status;
    status = jsonBody["estado_led"];  
    Serial.print("estado: "); 
    Serial.println(status);
    led_resource.status = status;
}

void post_method()
{
    StaticJsonBuffer<500> jsonBuffer;
    String post_body = http_rest_server.arg("plain");
    Serial.println(post_body);

    JsonObject& jsonBody = jsonBuffer.parseObject(http_rest_server.arg("plain"));

    Serial.print("HTTP Method: ");
    Serial.println(http_rest_server.method());
    
    if (jsonBody.success()) 
    {   
        if (http_rest_server.method() == HTTP_POST)
        {
            if (jsonBody["status"] > 1)
            {
                http_rest_server.send(201);
            }
            else
            {
                json_to_resource(jsonBody);
                http_rest_server.sendHeader("Location", "/leds/");
                http_rest_server.send(200);
                digitalWrite(PIN_LED, led_resource.status);
            }
        }
    }
    else
    {
        Serial.println("error in parsin json body");
        http_rest_server.send(400);
    }
}


void get_hello()
{                
  http_rest_server.send(200, "text/html","Bienvenido a ESP8266 REST Web Server");
}

void config_rest_server_routing() {
    http_rest_server.on("/", HTTP_GET, get_hello);
    http_rest_server.on("/leds", HTTP_GET, get_method);
    http_rest_server.on("/leds", HTTP_POST, post_method);
}

void setup(void)
{
    Serial.begin(9600);
    
    pinMode(PIN_LED,OUTPUT);
    
    
    init_led_resource();
    
    if (init_wifi() == WL_CONNECTED)
    {
        Serial.print("Conectando a  ");
        Serial.print(wifi_ssid);
        Serial.print("--- IP: ");
        Serial.println(WiFi.localIP());
    }
    else 
    {
        Serial.print("Error al conectar: ");
        Serial.println(wifi_ssid);
    }

    config_rest_server_routing();

    http_rest_server.begin();
    Serial.println("HTTP REST Server iniciado");
}

void loop(void)
{
    http_rest_server.handleClient();
}
