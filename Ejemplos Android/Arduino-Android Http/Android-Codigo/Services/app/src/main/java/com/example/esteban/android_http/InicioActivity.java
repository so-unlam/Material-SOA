/*********************************************************************************************************
 * Activity Inicial de la App que permite al usuario ingresar la IP y puerto del Servidor.
 * Contiene el codigo del front-end de la aplicacion
 **********************************************************************************************************/

package com.example.esteban.android_http;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

//Clase de la activty Inicial
public class InicioActivity extends AppCompatActivity {

    //Elementos graficos de la Activity
    Button btnOk;
    TextView txtIpServidor,txtPuertoServidor;

    //se crea un array de String con los permisos a solicitar en tiempo de ejecucion
    //Esto se debe realizar a partir de Android 6.0, ya que con verdiones anteriores
    //con solo solicitarlos en el Manifest es suficiente
    String[] permissions= new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.READ_CONTACTS};

    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        //Se asoscia cada componente de la activity a una variable
        btnOk =(Button)findViewById(R.id.btnOK);
        txtIpServidor=(TextView)findViewById(R.id.txtIpServidor);
        txtPuertoServidor=(TextView)findViewById(R.id.txtPuertoServidor);

        if (checkPermissions())
        {
            enableComponent();
        }

        }

    protected  void enableComponent()
    {
        //Se establece el handlers para el boton OK
        btnOk.setOnClickListener(botonesListeners);
    }

        //Metodo que es llamada cuando se cierra la activity
    protected void onDestroy()
    {
        super.onDestroy();
        //Toast.makeText(getApplicationContext(),"Cerrando Inicio",Toast.LENGTH_LONG).show();
    }

    //Metodo que actua como Listener de los eventos que ocurren en los componentes graficos de la activty
    private View.OnClickListener botonesListeners = new View.OnClickListener()
    {

        public void onClick(View v)
        {
            Intent intent;

            //Se determina que componente genero un evento
            switch (v.getId())
            {
                //Si se ocurrio un evento en el boton OK
               case R.id.btnOK:
                   //se genera un Intent para poder lanzar la activity principal
                   intent=new Intent(InicioActivity.this,PrincipalActivity.class);

                   //Se le agrega al intent los parametros que se le quieren pasar a la activyt principal
                   //cuando se lanzado
                   intent.putExtra("ipServidor",txtIpServidor.getText().toString());
                   intent.putExtra("puertoServidor",txtPuertoServidor.getText().toString());

                   //se inicia la activity principal
                   startActivity(intent);

                   //se cierra la acitvity de Inicio
                   finish();

                   break;
               default:
                Toast.makeText(getApplicationContext(),"Error en Listener de botones",Toast.LENGTH_LONG).show();
            }


        }
    };

    //Metodo que chequea si estan habilitados los permisos
    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();

        //Se chequea si la version de Android es menor a la 6
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }


        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permissions granted.
                    enableComponent(); // Now you call here what ever you want :)
                } else {
                    String perStr = "";
                    for (String per : permissions) {
                        perStr += "\n" + per;
                    }
                    // permissions list of don't granted permission
                    Toast.makeText(this,"ATENCION: La aplicacion no funcionara " +
                            "correctamente debido a la falta de Permisos", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


}

