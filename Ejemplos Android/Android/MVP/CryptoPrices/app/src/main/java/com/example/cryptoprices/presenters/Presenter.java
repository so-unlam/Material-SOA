package com.example.cryptoprices.presenters;

import com.example.cryptoprices.models.dtos.TokenResponse;

import java.util.List;

public interface Presenter {
    interface View {
        void response(List<TokenResponse> data);
    }

    interface Model {
        void getToken(String tokenCode);
        void getAllTokens();
    }
}
