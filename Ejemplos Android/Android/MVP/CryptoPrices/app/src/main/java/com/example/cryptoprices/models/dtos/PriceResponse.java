package com.example.cryptoprices.models.dtos;

public class PriceResponse {
    TokenResponse data;

    public TokenResponse getData() {
        return data;
    }

    public void setData(TokenResponse data) {
        this.data = data;
    }
}
