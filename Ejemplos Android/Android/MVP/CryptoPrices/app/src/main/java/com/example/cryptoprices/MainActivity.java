package com.example.cryptoprices;

import android.os.Bundle;

import com.example.cryptoprices.presenters.MainPresenter;
import com.example.cryptoprices.presenters.Presenter;
import com.example.cryptoprices.models.dtos.TokenResponse;
import com.example.cryptoprices.views.SearchPopup;
import com.example.cryptoprices.views.TokenAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Presenter.View {

    private static String TAG = MainActivity.class.getSimpleName();
    private Presenter.Model presenter;
    private ListView listView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textView = findViewById(R.id.main_textview);
        listView = findViewById(R.id.main_listview);
        listView.setAdapter(new TokenAdapter(new ArrayList<TokenResponse>(), this));

        presenter = new MainPresenter(this);
        presenter.getAllTokens();

        SearchPopup popup = new SearchPopup(presenter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.show(getSupportFragmentManager(), getString(R.string.popup_tag));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reset) {
            presenter.getAllTokens();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void response(List<TokenResponse> data) {
        if (data == null) {
            textView.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            Snackbar.make(listView, getString(R.string.token_not_found), Snackbar.LENGTH_LONG).show();
            return;
        }

        textView.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        listView.setAdapter(new TokenAdapter(data, this));
    }
}
