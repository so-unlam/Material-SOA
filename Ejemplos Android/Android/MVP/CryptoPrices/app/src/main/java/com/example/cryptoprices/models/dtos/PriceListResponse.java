package com.example.cryptoprices.models.dtos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PriceListResponse {
    HashMap<String, TokenResponse> data;

    public List<TokenResponse> getData() {
        List<TokenResponse> responses = new ArrayList<>();
        for (String key : data.keySet()) {
            responses.add(data.get(key));
        }
        return responses;
    }

    public void setData(HashMap<String, TokenResponse> data) {
        this.data = data;
    }
}
