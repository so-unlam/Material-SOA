package com.example.cryptoprices.models.services;

import com.example.cryptoprices.models.dtos.PriceListResponse;
import com.example.cryptoprices.models.dtos.PriceResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PancakeService {
    @GET("tokens/{tokenCode}")
    Call<PriceResponse> getToken(@Path("tokenCode") String tokenCode);

    @GET("tokens")
    Call<PriceListResponse> getAllTokens();
}
