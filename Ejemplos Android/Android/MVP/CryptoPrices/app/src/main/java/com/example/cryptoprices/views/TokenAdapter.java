package com.example.cryptoprices.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.cryptoprices.R;
import com.example.cryptoprices.models.dtos.TokenResponse;

import java.math.RoundingMode;
import java.util.List;

public class TokenAdapter extends BaseAdapter {

    private List<TokenResponse> tokens;
    private Context context;

    public TokenAdapter(List<TokenResponse> tokens, Context context) {
        this.tokens = tokens;
        this.context = context;
    }

    @Override
    public int getCount() {
        return tokens.size();
    }

    @Override
    public Object getItem(int position) {
        return tokens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        if (convertView == null) {
            v = LayoutInflater.from(context).inflate(R.layout.layout_token_item, null);
        } else {
            v = convertView;
        }

        TokenResponse response = tokens.get(position);

        TextView textViewName = v.findViewById(R.id.listview_item_name);
        TextView textViewSymbol = v.findViewById(R.id.listview_item_symbol);
        TextView textViewPrice = v.findViewById(R.id.listview_item_price);
        textViewName.setText(response.getName());
        textViewSymbol.setText(" - (" + response.getSymbol() + ")");
        textViewPrice.setText(response.getPrice().setScale(2, RoundingMode.HALF_UP) + "");

        return v;
    }
}
