package com.example.cryptoprices.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

import com.example.cryptoprices.R;
import com.example.cryptoprices.presenters.Presenter;

public class SearchPopup extends DialogFragment {
    private static String TAG = SearchPopup.class.getSimpleName();
    private Presenter.Model presenter;

    public SearchPopup(Presenter.Model presenter) {
        this.presenter = presenter;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.layout_popup_search, null));
        builder.setMessage(R.string.popup_title)
                .setPositiveButton(R.string.search, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.e(TAG, "BEGIN - Buscar Token");
                        EditText editText = getDialog().findViewById(R.id.popup_edit_text);
                        presenter.getToken(editText.getText() + "");
                        Log.e(TAG, "ENDED - Buscar Token");
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getDialog().cancel();
                    }
                })
                .setCancelable(false);

        return builder.create();
    }
}