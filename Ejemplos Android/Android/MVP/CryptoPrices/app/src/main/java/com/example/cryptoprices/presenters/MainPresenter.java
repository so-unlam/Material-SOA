package com.example.cryptoprices.presenters;

import android.util.Log;

import com.example.cryptoprices.MainActivity;
import com.example.cryptoprices.R;
import com.example.cryptoprices.models.dtos.PriceListResponse;
import com.example.cryptoprices.models.dtos.PriceResponse;
import com.example.cryptoprices.models.services.PancakeService;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainPresenter implements Presenter.Model {

    private static String TAG = MainPresenter.class.getSimpleName();

    private MainActivity activity;
    private PancakeService service;

    public MainPresenter(MainActivity activity) {
        this.activity = activity;
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(activity.getString(R.string.PANCAKE_API))
                .build();

        this.service = retrofit.create(PancakeService.class);
    }

    @Override
    public void getToken(String tokenCode) {
        Call<PriceResponse> execute = service.getToken(tokenCode);
        Log.e(TAG, "BEGIN - getToken");
        execute.enqueue(new Callback<PriceResponse>() {
            @Override
            public void onResponse(Call<PriceResponse> call, Response<PriceResponse> response) {
                if (response.isSuccessful()) {
                    activity.response(Arrays.asList(response.body().getData()));
                } else {
                    activity.response(null);
                }
                Log.e(TAG, response.message());
                Log.e(TAG, "ENDED - getToken");
            }

            @Override
            public void onFailure(Call<PriceResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }
        });
    }

    @Override
    public void getAllTokens() {
        Call<PriceListResponse> execute = service.getAllTokens();
        Log.e(TAG, "BEGIN - getAllTokens");
        execute.enqueue(new Callback<PriceListResponse>() {
            @Override
            public void onResponse(Call<PriceListResponse> call, Response<PriceListResponse> response) {
                if (response.isSuccessful()) {
                    activity.response(response.body().getData());
                }
                Log.e(TAG, response.message());
                Log.e(TAG, "ENDED - getAllTokens");
            }

            @Override
            public void onFailure(Call<PriceListResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }
        });
    }
}
