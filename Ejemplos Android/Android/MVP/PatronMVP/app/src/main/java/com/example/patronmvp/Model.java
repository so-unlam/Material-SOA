package com.example.patronmvp;

import java.util.List;
import java.util.Arrays;
import java.util.Random;

public class Model implements Contract.Model {

    private List<String> arrayList = Arrays.asList(
            "Juan", "Pedro", "Braulio", "Clemente","Marta", "Silvia", "Paula"
    );

    @Override
    public void getNextName(final OnEventListener listener) {
        listener.onEvent(getRandomString());
    }

    private String getRandomString() {
        Random random = new Random();
        int index = random.nextInt(arrayList.size());
        return arrayList.get(index);
    }
}
