package com.example.patronmvp;

public class Presenter implements Contract.Presenter, Contract.Model.OnEventListener{

    private Contract.View mainView;
    private Contract.Model model;

    public Presenter(Contract.View mainView, Contract.Model model){
        this.mainView = mainView;
        this.model = model;
    }

    @Override
    public void onButtonClick() {
        model.getNextName(this);
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void onEvent(String string){
        if(mainView != null){
            mainView.setString(string);
        }
    }
}
