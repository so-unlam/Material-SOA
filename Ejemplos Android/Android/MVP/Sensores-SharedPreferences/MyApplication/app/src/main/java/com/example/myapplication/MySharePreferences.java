package com.example.myapplication;

import android.content.SharedPreferences;

public class MySharePreferences {

    SharedPreferences preferences;

    public MySharePreferences(MainActivity activity) {
        preferences = activity.getApplicationContext().getSharedPreferences("DATOS", MainActivity.MODE_PRIVATE);
    }

    public void guardar(String text) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("key_bool", true); // Storing boolean - true/false
        editor.putString("key_string", text); // Storing string
        editor.putInt("key_int", 1); // Storing integer
        editor.putFloat("key_float", 2); // Storing float
        editor.putLong("key_long", 1L); // Storing long

        editor.commit();
    }

    public String getString(String tag) {
        return preferences.getString("key_string", "");
    }
}
