package com.example.myapplication;

public interface Main {
    interface View {
        void load(String text);
        void load2(String text);
    }

    interface Presenter {
        void process(String editText);
        void process2(String editText);
    }
}
