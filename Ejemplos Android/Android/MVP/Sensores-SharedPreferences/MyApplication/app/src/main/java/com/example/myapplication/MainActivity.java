package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements Main.View {

    private EditText editText;
    private Button button;
    private Button button2;
    private TextView textView;
    private TextView textView2;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.main_text);
        editText = findViewById(R.id.main_edit);
        button = findViewById(R.id.main_button);
        button2 = findViewById(R.id.main_button2);
        textView2 = findViewById(R.id.main_text2);

        presenter = new MainPresenter(this);

        button.setOnClickListener(v -> {
            presenter.process(editText.getText() + "");

        });

        button2.setOnClickListener(v -> {
            presenter.process2(editText.getText() + "");
        });
        presenter.setupSensorManager();
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.listenerSensors();
    }

    @Override
    public void load(String text) {
        this.textView.setText(text);
    }

    @Override
    public void load2(String text) {
        this.textView2.setText(text);
    }
}
