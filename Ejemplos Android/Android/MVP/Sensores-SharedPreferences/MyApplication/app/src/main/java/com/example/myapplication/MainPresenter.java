package com.example.myapplication;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.SystemClock;

import java.util.Arrays;
import java.util.List;

public class MainPresenter implements Main.Presenter, SensorEventListener {

    private MainActivity activity;
    private SensorManager sensorManager;
    private MySharePreferences preferences;

    public MainPresenter(MainActivity activity) {
        this.activity = activity;
        preferences = new MySharePreferences(activity);
    }

    public void setupSensorManager() {
        sensorManager = (SensorManager) this.activity.getSystemService(Context.SENSOR_SERVICE);
    }

    public void listenerSensors() {
        Sensor accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void process(String editText) {
        preferences.guardar(editText);

        MyThread  th = new MyThread(this.activity);
        th.execute();


    }

    @Override
    public void process2(String editText) {
        String guardado = preferences.getString("key_string");
        this.activity.load2(guardado);
    }



    @Override
    public void onSensorChanged(SensorEvent event) {

        synchronized (this) {
            float values = event.values[0];
            this.activity.load(values + "");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public class MyThread extends AsyncTask<Void, Void, Integer> {

        Main.View view;

        public MyThread(Main.View view) {
            this.view = view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            SystemClock.sleep(5000);

            return 6;
        }

        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);

            this.view.load(aVoid + "");
        }
    }
}
