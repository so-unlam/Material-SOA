package com.example.notificationservice;

import android.app.ForegroundServiceStartNotAllowedException;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.ServiceCompat;

public class NotificationService extends Service {
    private static final int ONGOING_NOTIFICATION_ID = 1;
    private static final String TAG = "NotificationService";
    private NotificationManagerCompat notificationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try{
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", "PennSkanvTicChannel", NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("Canal de notificación");
            notificationManager = NotificationManagerCompat.from(this);

            notificationManager.createNotificationChannel(channel);
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE);
            Notification notification =
                    new Notification.Builder(this, "CHANNEL_ID")
                            .setContentIntent(pendingIntent)
                            .build();
            startForeground(ONGOING_NOTIFICATION_ID, notification);
            runTimmer();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void runTimmer(){
        try {
            while (true){
                Thread.sleep(10000);
                Log.i(TAG, "Pasaron 10 segundos");
                //Esto se ejecuta en segundo plano
            }

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
