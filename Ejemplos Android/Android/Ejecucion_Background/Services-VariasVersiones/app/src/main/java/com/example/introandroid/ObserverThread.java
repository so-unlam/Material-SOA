package com.example.introandroid;

public interface ObserverThread {
    void notifyThreadComplete(final Thread thread);
}
