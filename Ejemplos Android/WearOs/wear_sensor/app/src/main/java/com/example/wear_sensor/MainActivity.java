package com.example.wear_sensor;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wear_sensor.databinding.ActivityMainBinding;

import java.text.DecimalFormat;

public class MainActivity extends Activity implements SensorEventListener {

    private SensorManager mSensorManager;
    private TextView txtLight;
    private TextView txtTwist;
    private TextView txtHeartRate;
    private float ant_x=0;
    DecimalFormat         dosdecimales = new DecimalFormat("###.###");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        txtLight    =(TextView) findViewById(R.id.txtLight);
        txtHeartRate=(TextView) findViewById(R.id.txtHeatRate);
        txtTwist    =(TextView) findViewById(R.id.txtTwist);

        // Accedemos al servicio de sensores
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

    }

    // Metodo para iniciar el acceso a los sensores
    protected void Ini_Sensores()
    {
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),   SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT),           SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE),        SensorManager.SENSOR_DELAY_NORMAL);
    }

    // Metodo para parar la escucha de los sensores
    private void Parar_Sensores()
    {

        mSensorManager.unregisterListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        mSensorManager.unregisterListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE));
        mSensorManager.unregisterListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT));
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        String txt = "";

        // Cada sensor puede lanzar un thread que pase por aqui
        // Para asegurarnos ante los accesos simult�neos sincronizamos esto

        synchronized (this) {
            Log.d("sensor", event.sensor.getName());

            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER :
                    txt += "x: " + dosdecimales.format(event.values[1]) + " ";
                    //txt += "y: " + dosdecimales.format(event.values[1]) + " ";
                    //txt += "z: " + dosdecimales.format(event.values[2]) + " ";

                    txtTwist.setText(txt);
                    if(checkTwistSWrist(event.values[1]))
                        Toast.makeText(getApplicationContext(), "Detecta Giro", Toast.LENGTH_SHORT).show();
                       // txtTwist.setText("0");
                    break;
                case Sensor.TYPE_HEART_RATE:
                    txt= dosdecimales.format(event.values[0]);
                    txtHeartRate.setText(txt);
                    break;
                case Sensor.TYPE_LIGHT:
                    txt= dosdecimales.format(event.values[0]);
                    txtLight.setText(txt);
                    break;
                default:
                    break;

            }
        }

    }

    private boolean checkTwistSWrist(float x) {
        if(ant_x==0 && x<-9)
        {
            ant_x=x;
            return true;
        }
        if(x==0){
            ant_x=x;
        }
        return false;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onRestart()
    {
        Ini_Sensores();

        super.onRestart();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        Ini_Sensores();
    }


}