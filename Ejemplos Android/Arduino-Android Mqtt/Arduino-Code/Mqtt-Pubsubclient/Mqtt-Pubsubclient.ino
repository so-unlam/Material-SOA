
#include <WiFi.h>
#include "PubSubClient.h"
#include <ArduinoJson.h>

#define DESACTIVATE '0' 
#define ACTIVATE    '1'


const char* ssid        = "SO Avanzados";
const char* password    = "SOA.2019";
const char* mqttServer  = "broker.emqx.io";
const char* user_name   = "";
const char* user_pass   = "";

const char * topic_temp  = "/casa/temperatura";
const char * topic_luz = "/casa/luz";

int port = 1883;
String stMac;
char mac[50];
char clientId[50];
long last_time= millis();

WiFiClient espClient;
PubSubClient client(espClient);

const int ledPin = 2;


void setup() 
{
  Serial.begin(9600);
 
  delay(10);
  
  Serial.println();
  Serial.print("Conectando a: ");
  Serial.println(ssid);

  wifiConnect();

  Serial.println("");
  Serial.println("WiFi Conectado");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println(WiFi.macAddress());
  stMac = WiFi.macAddress();
  stMac.replace(":", "_");
  Serial.println(stMac);
  
  client.setServer(mqttServer, port);
  client.setCallback(callback);
  pinMode(ledPin, OUTPUT);

  // La semilla aleatoria es un número que se utiliza para inicializar un generador de números pseudoaleatorios.
  randomSeed(analogRead(33));
}


void wifiConnect()  
{
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    Serial.print(".");
  }
}

//Funcion que reconecta el cliente, si pierde la conexion
void mqttReconnect() 
{
    if(!client.connected())
    {
      Serial.print("Intentando conexión MQTT...");
      long r = random(1000);
      sprintf(clientId, "clientId-%ld", r);
      if (client.connect(clientId,user_name,user_pass)) 
	  {
        Serial.print(clientId);
        Serial.println(" conectado");
        Serial.println("envio");
        client.subscribe(topic_luz);
      } else 
	  {
        Serial.print("fallo, rc=");
        Serial.print(client.state());
        Serial.println("intentando de nuevo en 5 segundos");
        delay(5000);
      }
  }
}

//Funcion Callback que recibe los mensajes enviados por lo dispositivos
void callback(char* topic, byte* message, unsigned int length) 
{
  char cMessage=char(*message);

  Serial.print("Se recibio mensaje en el topico: ");
  Serial.println(topic);
  Serial.print("Mensaje Recibido: ");
  Serial.println(cMessage);
  Serial.println();
  
  if(cMessage== ACTIVATE)
    digitalWrite(ledPin, HIGH);
  else
    digitalWrite(ledPin, LOW);

}

String generateJson()
{
    StaticJsonDocument<512> doc;
    String json;
    
    json="";

    doc["value"]=String(random(50));   
    serializeJson(doc,json) ;           

    return json;
}

void loop() 
{
  String json;
  char charMsgToSend[3];

  delay(10);
  if (!client.connected()) 
  {
    mqttReconnect();
  }

  
  long now = millis();
  if (now - last_time > 10000) 
  {
    
    json=generateJson();
    json.toCharArray(charMsgToSend, json.length()+1);
	
	//Se publica un mensaje en un topico del broker
    client.publish(topic_temp,(const char *)charMsgToSend);

    Serial.println("envio a Broker: ");
    Serial.println(charMsgToSend);
    Serial.println("");
    
    last_time = now;
  }

  client.loop();
}
