
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

DynamicJsonDocument doc(1024);
String json;

const char* ssid = "Maria";
const char* password = "44843368";
  
String token = "BBFF-KlzMyMgB7jnNEgxnrFVIUkHKJ0hVan";

String idPulsador="64c84cff64a4f5000dc5eeff";

// Domain Name with full URL Path for HTTP POST Request
String serverName = "https://industrial.api.ubidots.com/api/v1.6/devices/asaa/pulsador/lv";
// Service API Key

// THE DEFAULT TIMER IS SET TO 10 SECONDS FOR TESTING PURPOSES
// For a final application, check the API call limits per hour/minute to avoid getting blocked/banned
unsigned long lastTime = 0;

unsigned long timerDelay = 5000;

void setup() {
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
  Serial.println("Timer set to 10 seconds (timerDelay variable), it will take 10 second the first reading.");

  // Random seed is a number used to initialize a pseudorandom number generator
  randomSeed(analogRead(33));
}

void loop() {
  //Send an HTTP POST request every 10 seconds
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
      WiFiClient client;
      HTTPClient http;
          
      // Your Domain name with URL path or IP address with path
      http.begin(serverName);
      
      // Specify content-type header
      http.addHeader("Content-Type", "application/json");
      http.addHeader("X-Auth-Token", token);


      Serial.println("Antes de GET");
      int httpResponseCode = http.GET();
      Serial.println("Despues de GET");
      
      Serial.println(serverName);  
      if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
      }
  
      // Free resources
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}