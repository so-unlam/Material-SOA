Codigos de ejemplos para dispositvos Android desarrollados para los alumnos de la catedra SOA de la UNLaM.
En los siguientes ejemplos se puede encontrar codigo de las siguientes aplicaciones para se compiladas
desde Android Studio:
 
Activity:
=========
	-Ejemplo del Ciclo de Vida de una Activity
	-Ejemplo de paso de parametros entre Activity
	-Ejemplo de guardado de estado de una Activity utilizando Bundle
 
Sensores:
=========
	-Ejemplo de Listado de Sensores
	-Ejemplo de un Shake
	
Ejecucion en Background:
=========
	-Ejemplo de Thread usando Runnable
	-Ejemplo de Thread usando Asynctask
	-Ejemplo de Thread usando Services
	
Conectividad entre dispositivos Android y Arduino/ESP32:
=========================================================
	-Ejemplo Mqtt (ESP32)
	-Ejemplo Firebase (ESP32)
	-Ejemplo Bluetooth (Arduino)
	-Ejemplo socket UDP (Arduino)
	-Ejemplo de servicio Rest (Local) (Arduino)

	
	
